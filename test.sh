#!/bin/bash

CURR_DIR=${PWD}
PROJECT_DIR=/home/eldius/dev/workspaces/java/feed-tools-api

VERSION=0.0.10
FILE_NAME=feed-api-${VERSION}_all.deb

function download_package() {
    wget https://bintray.com/eldius/feed-tools-deb/download_file?file_path=${FILE_NAME} -O ${FILE_NAME}
}

function build_package() {
    cd $PROJECT_DIR
    ./mvnw clean package
    cp $PROJECT_DIR/target/*.deb $CURR_DIR/$FILE_NAME
    cd $CURR_DIR
}

function get_package() {
    echo ""
    echo "#################################################"
    echo "## Acquiring package                           ##"
    echo "#################################################"
    echo ""

    rm $FILE_NAME

    build_package
}

vagrant destroy -f

#get_package

#dpkg -i $FILE_NAME

#ls -la *.deb
#rm $FILE_NAME

echo ""
echo ""

vagrant up && vagrant ssh api
