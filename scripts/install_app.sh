#!/bin/bash

echo ""
echo "#################################################"
echo "## Downloading package                         ##"
echo "#################################################"
echo ""

VERSION=0.0.11
FILE_NAME=feed-fetcher-deb-systemd_${VERSION}_all.deb
mysql_fetcher_host=database
mysql_fetcher_pass=123Senha

cd /tmp

wget https://bintray.com/eldius/feed-tools-deb/download_file?file_path=${FILE_NAME} -O ${FILE_NAME}

ls -la *.deb

dpkg -i ${FILE_NAME}

FETCHER_CONFIG_FILE=/usr/share/feed-fetcher-deb-systemd/conf/application.conf
sed -i "s/localhost/${mysql_fetcher_host}/g" ${FETCHER_CONFIG_FILE}
sed -i "s/123Senha/${mysql_fetcher_pass}/g" ${FETCHER_CONFIG_FILE}

echo ""
echo ""
