#!/bin/bash

echo ""
echo "#################################################"
echo "## Installing REST API package                 ##"
echo "#################################################"
echo ""

api_version=0.0.1-ALFA
mysql_api_host=database
API_FILE_NAME=feed-api-${api_version}_all.deb

TMP_FOLDER=mktemp -d

cd $TMP_FOLDER
wget https://bintray.com/eldius/feed-tools-deb/download_file?file_path=${API_FILE_NAME} -O ${API_FILE_NAME}

ls -la *.deb

mysql_api_pass=123Senha

dpkg -i ${API_FILE_NAME}

API_CONFIG_FILE=/usr/share/feed-api/config/application.properties
sed -i "s/dbpass/${mysql_api_pass}/g" ${API_CONFIG_FILE}
sed -i "s/localhost/${mysql_api_host}/g" ${API_CONFIG_FILE}

rm -rf $TMP_FOLDER

systemctl enable feed-api
systemctl restart feed-api

echo ""
echo ""
