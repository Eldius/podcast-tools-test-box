#!/bin/sh

export FLUENTD_SECRET=MY_STRONG_FLUENTD_PASS

apt-get update

cd /tmp

wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.2.4.deb

dpkg -i elasticsearch-6.2.4.deb

sudo systemctl enable elasticsearch.service

echo "############################################"
echo "## TESTING ELASTICSEARCH ###################"
echo ""
curl -i http://localhost:9200/
echo ""
echo "############################################"

curl -L http://toolbelt.treasuredata.com/sh/install-ubuntu-xenial-td-agent2.sh | sh

sudo /usr/sbin/td-agent-gem install fluent-plugin-secure-forward
sudo /usr/sbin/td-agent-gem install fluent-plugin-elasticsearch

mv /etc/td-agent/td-agent.conf /etc/td-agent/td-agent.conf.bak

cat <<EOT >> /etc/td-agent/td-agent.conf
# Listen to incoming data over SSL
<source>
  type secure_forward
  shared_key ${FLUENTD_SECRET}
  self_hostname logs.example.com
  cert_auto_generate yes
</source>

# Store Data in Elasticsearch and S3
<match *.**>
  type copy
  <store>
    type elasticsearch
    host localhost
    port 9200
    include_tag_key true
    tag_key @log_name
    logstash_format true
    flush_interval 10s
  </store>
</match>

EOT

sudo service td-agent restart
