#!/bin/sh

apt-get update

cat << EOF | debconf-set-selections
    mysql-server-5.0 mysql-server/root_password password 123Senha
    mysql-server-5.0 mysql-server/root_password_again password 123Senha
    mysql-server-5.0 mysql-server/root_password seen true
    mysql-server-5.0 mysql-server/root_password_again seen true
EOF

apt-get install -y software-properties-common
apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
add-apt-repository 'deb [arch=amd64,i386,ppc64el] http://mirror.ufscar.br/mariadb/repo/10.2/ubuntu xenial main'

sed -i 's/127.0.0.1/0.0.0.0/g' /etc/mysql/my.cnf

apt-get update
apt-get install -y mariadb-server

mysql -u root --password=123Senha -t <<STOP
    -- This is a comment inside an sql-command-stream.
    create database development;
    create database auth_development;

    CREATE USER 'feedApp'@'localhost' IDENTIFIED BY '123Senha';
    CREATE USER 'feedApp'@'%' IDENTIFIED BY '123Senha';

    GRANT ALL ON *.* TO 'feedApp'@'localhost';
    GRANT ALL ON *.* TO 'feedApp'@'%';

    CREATE USER 'feedAuth'@'localhost' IDENTIFIED BY '123Senha';
    CREATE USER 'feedAuth'@'%' IDENTIFIED BY '123Senha';

    GRANT ALL ON *.* TO 'feedAuth'@'localhost';
    GRANT ALL ON *.* TO 'feedAuth'@'%';

    show databases;
    \q
STOP

test $? = 0 && echo "Your batch job terminated gracefully"

