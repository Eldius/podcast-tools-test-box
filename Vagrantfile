# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/xenial64"

#  config.vm.network "forwarded_port", guest: 6969, host: 6969

  config.vm.provision "shell", inline: <<-SHELL
    apt-get update
    apt-get upgrade -y
    apt-get install -y joe curl wget
  SHELL

  if Vagrant.has_plugin?("vagrant-cachier")
		config.cache.scope = :box

		config.cache.synced_folder_opts = {
		}
	end

  config.vm.define "db" do |db|
    db.vm.hostname = "database"
    db.vm.network "forwarded_port", guest: 3306, host: 3306
    db.vm.provision :shell, :path => "./scripts/mysql.sh"
    db.vm.network :private_network, ip: '192.168.42.10'
    if Vagrant.has_plugin?("vagrant-hosts")
      db.vm.provision :hosts do |provisioner|
        provisioner.sync_hosts = true
        provisioner.imports = ['virtualbox']
        provisioner.exports = {
          'virtualbox' => [
            ['@vagrant_private_networks', ['@vagrant_hostnames']],
          ],
        }
      end
    end
    db.vm.provider "virtualbox" do |vb|
      # Display the VirtualBox GUI when booting the machine
      vb.gui = false
      # Customize the amount of memory on the VM:
      vb.memory = "1024"
    end
  end

#  config.vm.define "kibana" do |kibana|
#    kibana.vm.provision :shell, :path => "./scripts/java.sh"
#    kibana.vm.provision :shell, :path => "./scripts/kibana.sh"
#    if Vagrant.has_plugin?("vagrant-hosts")
#    kibana.vm.provision :hosts do |provisioner|
#      provisioner.vm.provision :hosts, :sync_hosts => true
#      provisioner.exports = {
#        'virtualbox-kibana' => [
#          ['@vagrant_private_networks', ['@vagrant_hostnames']],
#        ],
#      }
#      provisioner.imports = ['virtualbox-kibana']
#    end
#    end
#    kibana.vm.provider "virtualbox" do |vb|
#      # Display the VirtualBox GUI when booting the machine
#      vb.gui = false
#      # Customize the amount of memory on the VM:
#      vb.memory = "3072"
#    end
#  end

  config.vm.define "fetcher" do |fetcher|
    fetcher.vm.hostname = "app-fetcher"
    fetcher.vm.provision :shell, :path => "./scripts/java.sh"
    fetcher.vm.provision :shell, :path => "./scripts/install_app.sh"
    fetcher.vm.network :private_network, ip: '192.168.42.11'
    if Vagrant.has_plugin?("vagrant-hosts")
      fetcher.vm.provision :hosts do |provisioner|
        provisioner.sync_hosts = true
        provisioner.imports = ['virtualbox']
        provisioner.exports = {
          'virtualbox' => [
            ['@vagrant_private_networks', ['@vagrant_hostnames']],
          ],
        }
      end
    end
    fetcher.vm.provider "virtualbox" do |vb|
      # Display the VirtualBox GUI when booting the machine
      vb.gui = false
      # Customize the amount of memory on the VM:
      vb.memory = "1024"
    end
  end

  config.vm.define "api" do |api|
    api.vm.hostname = "app-api"
    api.vm.provision :shell, :path => "./scripts/java.sh"
    api.vm.provision :shell, :path => "./scripts/install_api.sh"
    api.vm.network "forwarded_port", guest: 8080, host: 8080
    api.vm.network :private_network, ip: '192.168.42.12'
    if Vagrant.has_plugin?("vagrant-hosts")
      api.vm.provision :hosts do |provisioner|
        provisioner.sync_hosts = true
        provisioner.imports = ['virtualbox']
        provisioner.exports = {
          'virtualbox' => [
            ['@vagrant_private_networks', ['@vagrant_hostnames']],
          ],
        }
      end
    end
    api.vm.provider "virtualbox" do |vb|
      # Display the VirtualBox GUI when booting the machine
      vb.gui = false
      # Customize the amount of memory on the VM:
      vb.memory = "1024"
    end
  end
end
