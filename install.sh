#!/bin/bash

sudo apt-get purge -y feed-api-deb; \
    sudo rm /var/log/feed-api-deb/application.log; \
    sudo dpkg -i /vagrant/feed-api-deb_0.0.6-SNAPSHOT_all.deb && \
    sudo service feed-api-deb restart && \
    sleep 10 && \
    tail -f /var/log/feed-api-deb/application.log
