#!/usr/bin/env bash

API_KEY=$1

curl -i -X POST \
  http://localhost:8080/feed \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: da70a112-8c36-468d-bcd3-c06625b07cf0' \
  -H "X-API-KEY: ${API_KEY}" \
  -d '{
	"name": "NewsFeed"
	, "url": "https://jovemnerd.com.br/feed-nerdcast/"
}'

echo ""

curl -i -X POST \
  http://localhost:8080/feed \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: d65fd1d1-81da-4292-b6db-76bd9d9628da' \
  -H "X-API-KEY: ${API_KEY}" \
  -d '{
	"name": "NewsFeed"
	, "url": "http://pox.globo.com/rss/g1/"
}'

echo ""

curl -i -X POST \
  http://localhost:8080/feed \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: d65fd1d1-81da-4292-b6db-76bd9d9628da' \
  -H "X-API-KEY: ${API_KEY}" \
  -d '{
	"name": "NewsFeed"
	, "url": "https://rss.tecmundo.com.br/feed"
}'

echo ""

curl -i -X POST \
  http://localhost:8080/feed \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: d65fd1d1-81da-4292-b6db-76bd9d9628da' \
  -H "X-API-KEY: ${API_KEY}" \
  -d '{
	"name": "NewsFeed"
	, "url": "http://gizmodo.uol.com.br/feed/"
}'

echo ""

curl -i -X POST \
  http://localhost:8080/feed \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: d65fd1d1-81da-4292-b6db-76bd9d9628da' \
  -H "X-API-KEY: ${API_KEY}" \
  -d '{
	"name": "NewsFeed"
	, "url": "http://feeds.bbci.co.uk/portuguese/rss.xml"
}'


echo ""

curl -i -X POST \
  http://localhost:8080/feed \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: d65fd1d1-81da-4292-b6db-76bd9d9628da' \
  -H "X-API-KEY: ${API_KEY}" \
  -d '{
	"name": "NewsFeed"
	, "url": "http://www.bbc.com/portuguese/topicos/brasil/index.xml"
}'


echo ""

curl -i -X POST \
  http://localhost:8080/feed \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: d65fd1d1-81da-4292-b6db-76bd9d9628da' \
  -H "X-API-KEY: ${API_KEY}" \
  -d '{
	"name": "NewsFeed"
	, "url": "https://noticias.r7.com/feed.xml"
}'



echo ""

curl -i -X POST \
  http://localhost:8080/feed \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: d65fd1d1-81da-4292-b6db-76bd9d9628da' \
  -H "X-API-KEY: ${API_KEY}" \
  -d '{
	"name": "NewsFeed"
	, "url": "https://noticias.r7.com/rio-de-janeiro/feed.xml"
}'



echo ""

curl -i -X POST \
  http://localhost:8080/feed \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: d65fd1d1-81da-4292-b6db-76bd9d9628da' \
  -H "X-API-KEY: ${API_KEY}" \
  -d '{
	"name": "NewsFeed"
	, "url": "http://rss.home.uol.com.br/index.xml"
}'


echo ""

curl -i -X POST \
  http://localhost:8080/feed \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H "X-API-KEY: ${API_KEY}" \
  -H 'X-API-KEY: TEST_KEY_VALUE' \
  -d '{
	"name": "NewsFeed"
	, "url": "http://tecnologia.uol.com.br/ultnot/index.xml"
}'


echo ""

curl -i -X POST \
  http://localhost:8080/feed \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H "X-API-KEY: ${API_KEY}" \
  -H 'X-API-KEY: TEST_KEY_VALUE' \
  -d '{
	"name": "NewsFeed"
	, "url": "http://rss.uol.com.br/feed/economia.xml"
}'


echo ""

curl -i -X POST \
  http://localhost:8080/feed \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: d65fd1d1-81da-4292-b6db-76bd9d9628da' \
  -H "X-API-KEY: ${API_KEY}" \
  -d '{
	"name": "NewsFeed"
	, "url": "http://rss.uol.com.br/feed/noticias.xml"
}'


echo ""

curl -i -X POST \
  http://localhost:8080/feed \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: d65fd1d1-81da-4292-b6db-76bd9d9628da' \
  -H "X-API-KEY: ${API_KEY}" \
  -d '{
	"name": "NewsFeed"
	, "url": "http://feeds.bbci.co.uk/portuguese/rss.xml"
}'



echo ""
