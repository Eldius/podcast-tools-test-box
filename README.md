# INFO #

## Vagrant plugins ##

- vagrant-cachier
- vagrant-hosts

### Install plugins ###

    vagrant plugin install vagrant-hosts
    vagrant plugin install vagrant-cachier
