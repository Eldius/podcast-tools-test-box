#!/bin/bash

CURR_DIR=${PWD}

_now=$(date +%Y-%m-%d_%H)
MYSQL_NACKUP_NAME=backups-mysql_${_now}.tar.bz2

mkdir -p backup/mysql

ssh root@159.89.187.90 ./backup_mysql.sh
ssh root@159.89.187.90 tar -cvjf $MYSQL_NACKUP_NAME backups/
cd $CURR_DIR/backup/mysql
sftp root@159.89.187.90:$MYSQL_NACKUP_NAME
cd $CURR_DIR
ssh root@159.89.187.90 rm -f $MYSQL_NACKUP_NAME

ssh root@159.89.187.90 ls -la

ls -la backup/*
